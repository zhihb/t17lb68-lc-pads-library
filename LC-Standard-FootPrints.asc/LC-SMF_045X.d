*PADS-LIBRARY-PCB-DECALS-V9*

LC-SMF_045X      I 0     0     2 2 3 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V2
-155.94 136.74 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
-155.94 86.74 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
CLOSED 5 7.87 18 -1
-120.08 53.15
120.08 53.15
120.08 -53.15
-120.08 -53.15
-120.08 53.15
OPEN   4 7.87 26 -1
-47.24 74.8 
-152  74.8 
-152  -74.8
-47.24 -74.8
OPEN   4 7.87 26 -1
47.24 -74.8
152   -74.8
152   74.8 
47.24 74.8 
T100.39 0     100.39 0     1
T-100.39 0     -100.39 0     2
PAD 0 3 P 0    
-2 78.74 RF  0   90   125.98 0  
-1 0   R  
0  0   R  

*END*
