*PADS-LIBRARY-PCB-DECALS-V9*

LC-0201_R        I 0     0     2 2 2 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-32.09 29.65 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-32.09 79.65 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   4 7.87 26 -1
-9.84 -17.72
-28.15 -17.72
-28.15 17.72
-9.84 17.72
OPEN   4 7.87 26 -1
9.84  -17.72
28.15 -17.72
28.15 17.72
9.84  17.72
T-11.22 0     -11.22 0     1
T11.22 0     11.22 0     2
PAD 0 4 P 0    
-2 14.17 RF  0   90   15.75 0  
-1 0   R  
0  0   R  
21 18.17 RF  0   90   19.75 0  

*END*
