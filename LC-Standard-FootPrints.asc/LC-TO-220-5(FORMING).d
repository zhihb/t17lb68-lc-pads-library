*PADS-LIBRARY-PCB-DECALS-V9*

LC-TO-220-5(FORMING) I 0     0     2 2 16 0 5 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-203.94 328.6 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-203.94 378.6 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 15.75 26 -1
-169.29 69.34
-185.04 69.34
OPEN   2 7.87 26 -1
150   59.05
150   141.67
OPEN   2 7.87 26 -1
16.67 59.05
16.67 114.17
OPEN   2 7.87 26 -1
-16.67 59.05
-16.67 114.17
OPEN   2 7.87 26 -1
-150  59.05
-150  141.67
OPEN   2 7.87 26 -1
-58.33 291.67
-58.33 300  
OPEN   2 7.87 26 -1
58.33 291.67
58.33 300  
OPEN   2 7.87 26 -1
58.33 308.33
58.33 316.67
OPEN   2 7.87 26 -1
-58.33 308.33
-58.33 316.67
OPEN   2 7.87 26 -1
58.33 275  
58.33 283.33
OPEN   2 7.87 26 -1
-58.33 275  
-58.33 283.33
OPEN   2 7.87 26 -1
-200  275  
200   275  
OPEN   6 7.87 26 -1
125.46 141.67
200   141.67
200   316.67
-200  316.67
-200  141.67
-125.46 141.67
OPEN   2 8 26 -1
-15.75 137.8
15.75 137.8
OPEN   2 7.87 26 -1
117.87 59.05
117.87 125.98
OPEN   2 7.87 26 -1
-117.58 59.05
-117.58 125.98
T-133.33 0     -133.33 0     1
T-66.67 141.67 -66.67 141.67 2
T0     0     0     0     3
T66.67 141.67 66.67 141.67 4
T133.33 0     133.33 0     5
PAD 1 3 P 51.18
-2 78.74 S   0  
-1 78.74 S   0  
0  78.74 S   0  
PAD 0 3 P 51.18
-2 78.74 R  
-1 78.74 R  
0  78.74 R  

*END*
