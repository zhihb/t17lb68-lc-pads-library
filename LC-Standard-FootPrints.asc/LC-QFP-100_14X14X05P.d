*PADS-LIBRARY-PCB-DECALS-V9*

LC-QFP-100_14X14X05P I 0     0     2 2 5 0 100 51 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-334.68 342.68 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-334.68 392.68 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 11.81 26 -1
-255.91 -313.98
-267.72 -313.98
CIRCLE 2 23.62 26 -1
-194.88 -206.69
-218.5 -206.69
CIRCLE 2 3.94 18 -1
-220.47 -240.16
-259.84 -240.16
CLOSED 5 7.87 26 -1
-246.06 -246.06
246.06 -246.06
246.06 246.06
-246.06 246.06
-246.06 -246.06
CLOSED 5 3.94 18 -1
-275.59 -275.59
275.59 -275.59
275.59 275.59
-275.59 275.59
-275.59 -275.59
T-236.22 -297.24 -236.22 -297.24 1
T-216.54 -297.24 -216.54 -297.24 2
T-196.85 -297.24 -196.85 -297.24 3
T-177.17 -297.24 -177.17 -297.24 4
T-157.48 -297.24 -157.48 -297.24 5
T-137.8 -297.24 -137.8 -297.24 6
T-118.11 -297.24 -118.11 -297.24 7
T-98.43 -297.24 -98.43 -297.24 8
T-78.74 -297.24 -78.74 -297.24 9
T-59.05 -297.24 -59.05 -297.24 10
T-39.37 -297.24 -39.37 -297.24 11
T-19.68 -297.24 -19.68 -297.24 12
T0     -297.24 0     -297.24 13
T19.69 -297.24 19.69 -297.24 14
T39.37 -297.24 39.37 -297.24 15
T59.06 -297.24 59.06 -297.24 16
T78.74 -297.24 78.74 -297.24 17
T98.43 -297.24 98.43 -297.24 18
T118.11 -297.24 118.11 -297.24 19
T137.8 -297.24 137.8 -297.24 20
T157.48 -297.24 157.48 -297.24 21
T177.17 -297.24 177.17 -297.24 22
T196.85 -297.24 196.85 -297.24 23
T216.54 -297.24 216.54 -297.24 24
T236.22 -297.24 236.22 -297.24 25
T297.24 -236.22 297.24 -236.22 26
T297.24 -216.54 297.24 -216.54 27
T297.24 -196.85 297.24 -196.85 28
T297.24 -177.17 297.24 -177.17 29
T297.24 -157.48 297.24 -157.48 30
T297.24 -137.8 297.24 -137.8 31
T297.24 -118.11 297.24 -118.11 32
T297.24 -98.43 297.24 -98.43 33
T297.24 -78.74 297.24 -78.74 34
T297.24 -59.05 297.24 -59.05 35
T297.24 -39.37 297.24 -39.37 36
T297.24 -19.68 297.24 -19.68 37
T297.24 0     297.24 0     38
T297.24 19.69 297.24 19.69 39
T297.24 39.37 297.24 39.37 40
T297.24 59.06 297.24 59.06 41
T297.24 78.74 297.24 78.74 42
T297.24 98.43 297.24 98.43 43
T297.24 118.11 297.24 118.11 44
T297.24 137.8 297.24 137.8 45
T297.24 157.48 297.24 157.48 46
T297.24 177.17 297.24 177.17 47
T297.24 196.85 297.24 196.85 48
T297.24 216.54 297.24 216.54 49
T297.24 236.22 297.24 236.22 50
T236.22 297.24 236.22 297.24 51
T216.54 297.24 216.54 297.24 52
T196.85 297.24 196.85 297.24 53
T177.17 297.24 177.17 297.24 54
T157.48 297.24 157.48 297.24 55
T137.8 297.24 137.8 297.24 56
T118.11 297.24 118.11 297.24 57
T98.43 297.24 98.43 297.24 58
T78.74 297.24 78.74 297.24 59
T59.06 297.24 59.06 297.24 60
T39.37 297.24 39.37 297.24 61
T19.69 297.24 19.69 297.24 62
T0     297.24 0     297.24 63
T-19.68 297.24 -19.68 297.24 64
T-39.37 297.24 -39.37 297.24 65
T-59.05 297.24 -59.05 297.24 66
T-78.74 297.24 -78.74 297.24 67
T-98.43 297.24 -98.43 297.24 68
T-118.11 297.24 -118.11 297.24 69
T-137.8 297.24 -137.8 297.24 70
T-157.48 297.24 -157.48 297.24 71
T-177.17 297.24 -177.17 297.24 72
T-196.85 297.24 -196.85 297.24 73
T-216.54 297.24 -216.54 297.24 74
T-236.22 297.24 -236.22 297.24 75
T-297.24 236.22 -297.24 236.22 76
T-297.24 216.54 -297.24 216.54 77
T-297.24 196.85 -297.24 196.85 78
T-297.24 177.17 -297.24 177.17 79
T-297.24 157.48 -297.24 157.48 80
T-297.24 137.8 -297.24 137.8 81
T-297.24 118.11 -297.24 118.11 82
T-297.24 98.43 -297.24 98.43 83
T-297.24 78.74 -297.24 78.74 84
T-297.24 59.06 -297.24 59.06 85
T-297.24 39.37 -297.24 39.37 86
T-297.24 19.69 -297.24 19.69 87
T-297.24 0     -297.24 0     88
T-297.24 -19.68 -297.24 -19.68 89
T-297.24 -39.37 -297.24 -39.37 90
T-297.24 -59.05 -297.24 -59.05 91
T-297.24 -78.74 -297.24 -78.74 92
T-297.24 -98.43 -297.24 -98.43 93
T-297.24 -118.11 -297.24 -118.11 94
T-297.24 -137.8 -297.24 -137.8 95
T-297.24 -157.48 -297.24 -157.48 96
T-297.24 -177.17 -297.24 -177.17 97
T-297.24 -196.85 -297.24 -196.85 98
T-297.24 -216.54 -297.24 -216.54 99
T-297.24 -236.22 -297.24 -236.22 100
PAD 0 4 P 0    
-2 11.02 OF  90   70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   74.87 0  
PAD 26 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 27 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 28 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 29 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 30 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 31 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 32 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 33 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 34 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 35 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 36 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 37 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 38 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 39 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 40 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 41 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 42 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 43 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 44 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 45 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 46 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 47 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 48 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 49 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 50 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 76 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 77 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 78 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 79 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 80 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 81 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 82 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 83 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 84 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 85 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 86 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 87 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 88 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 89 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 90 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 91 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 92 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 93 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 94 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 95 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 96 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 97 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 98 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 99 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  
PAD 100 4 P 0    
-2 11.02 OF  0    70.87 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    74.87 0  

*END*
