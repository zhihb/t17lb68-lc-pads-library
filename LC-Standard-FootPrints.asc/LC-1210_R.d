*PADS-LIBRARY-PCB-DECALS-V9*

LC-1210_R        I 0     0     2 2 2 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-96.65 77.29 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-96.65 127.29 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   4 7.87 26 -1
-35.43 65.35
-92.72 65.35
-92.72 -65.35
-35.43 -65.35
OPEN   4 7.87 26 -1
35.43 -65.35
92.72 -65.35
92.72 65.35
35.43 65.35
T-58.07 0     -58.07 0     1
T58.07 0     58.07 0     2
PAD 0 3 P 0    
-2 45.67 RF  0   90   107.09 0  
-1 0   R  
0  0   R  

*END*
