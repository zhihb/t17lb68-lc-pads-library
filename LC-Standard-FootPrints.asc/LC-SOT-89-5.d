*PADS-LIBRARY-PCB-DECALS-V9*

LC-SOT-89-5      I 0     0     2 2 4 0 6 3 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-102.36 103.55 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-102.36 153.55 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 9.84 26 -1
81.69 -92.52
71.85 -92.52
OPEN   2 10 26 -1
-51.18 90.55
51.18 90.55
OPEN   2 10 26 -1
-51.18 -90.55
51.18 -90.55
CLOSED 5 3.94 18 -1
-50.98 90.55
50.98 90.55
50.98 -90.55
-50.98 -90.55
-50.98 90.55
T66.93 -59.06 66.93 -59.06 1
T55.12 0     55.12 0     2
T-29.53 0     -29.53 0     2/2
T66.93 59.06 66.93 59.06 3
T-66.93 59.06 -66.93 59.06 4
T-66.93 -59.06 -66.93 -59.06 5
PAD 0 3 P 0    
-2 22.83 OF  0    70.87 0  
-1 0   R  
0  0   R  
PAD 2 3 P 0    
-2 22.83 OF  0    94.49 0  
-1 0   R  
0  0   R  
PAD 3 3 P 0    
-2 70.87 OF  0    137.8 0  
-1 0   R  
0  0   R  

*END*
