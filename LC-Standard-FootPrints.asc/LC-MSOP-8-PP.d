*PADS-LIBRARY-PCB-DECALS-V9*

LC-MSOP-8-PP     I 0     0     2 2 4 0 9 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-78.74 126.14 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-78.74 176.14 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 11.81 26 -1
-61.02 -98.43
-72.83 -98.43
CLOSED 5 3.94 18 -1
61.02 -61.02
61.02 61.02
-61.02 61.02
-61.02 -61.02
61.02 -61.02
OPEN   2 10 26 -1
-62.99 -47.24
-62.99 43.31
OPEN   2 10 26 -1
62.99 -43.31
62.99 47.24
T0     0     0     0     0
T-38.39 -82.68 -38.39 -82.68 1
T-12.8 -82.68 -12.8 -82.68 2
T12.8  -82.68 12.8  -82.68 3
T38.39 -82.68 38.39 -82.68 4
T38.39 82.68 38.39 82.68 5
T12.8  82.68 12.8  82.68 6
T-12.8 82.68 -12.8 82.68 7
T-38.39 82.68 -38.39 82.68 8
PAD 1 4 P 0    
-2 70.87 RF  0   0    98.43 0  
-1 0   R  
0  0   R  
21 74.87 RF  0   0    102.43 0  
PAD 0 4 P 0    
-2 16.93 OF  90   66.93 0  
-1 0   R  
0  0   R  
21 20.93 OF  90   70.93 0  

*END*
