*PADS-LIBRARY-PCB-DECALS-V9*

LC-2010_L        I 0     0     2 2 2 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-131.5 76.31 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-131.5 126.31 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 1.97 18 -1
-98.43 49.21
98.43 49.21
98.43 -49.21
-98.43 -49.21
-98.43 49.21
CLOSED 5 7.87 26 -1
-127.56 -64.37
127.56 -64.37
127.56 64.37
-127.56 64.37
-127.56 -64.37
T-92.13 0     -92.13 0     1
T92.13 0     92.13 0     2
PAD 0 3 P 0    
-2 47.24 RF  0   90   105.12 0  
-1 0   R  
0  0   R  

*END*
