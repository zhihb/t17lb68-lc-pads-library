*PADS-LIBRARY-PCB-DECALS-V9*

LC-SC-101        I 0     0     2 2 3 0 3 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-30.54 37.53 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-30.54 87.53 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 2 18 -1
-19.68 12.2 
19.68 12.2 
19.68 -12.2
-19.68 -12.2
-19.68 12.2 
OPEN   2 7.87 26 -1
-19.68 25.59
19.68 25.59
OPEN   2 7.87 26 -1
-19.68 -25.59
19.68 -25.59
T16.73 -8.86 16.73 -8.86 1
T16.73 8.86  16.73 8.86  2
T-16.73 0     -16.73 0     3
PAD 0 4 P 0    
-2 9.84 RF  0   0    23.62 0  
-1 0   R  
0  0   R  
21 13.84 RF  0   0    27.62 0  
PAD 3 4 P 0    
-2 23.62 RF  0   90   27.56 0  
-1 0   R  
0  0   R  
21 27.62 RF  0   90   31.56 0  

*END*
