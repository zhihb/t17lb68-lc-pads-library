*PADS-LIBRARY-PCB-DECALS-V9*

LC-SOT-666       I 0     0     2 2 4 0 6 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-37.43 50.4  0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-37.43 100.4 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 8 26 -1
42.87 -43.31
35.87 -43.31
OPEN   2 10 26 -1
-26.38 -37.4
23.62 -37.4
OPEN   2 10 26 -1
-26.38 37.4 
23.62 37.4 
CLOSED 5 3.94 18 -1
-23.62 -31.5
23.62 -31.5
23.62 31.5 
-23.62 31.5 
-23.62 -31.5
T23.62 -19.68 23.62 -19.68 1
T23.62 0     23.62 0     2
T23.62 19.69 23.62 19.69 3
T-23.62 19.69 -23.62 19.69 4
T-23.62 0     -23.62 0     5
T-23.62 -19.68 -23.62 -19.68 6
PAD 0 4 P 0    
-2 9.84 RF  0   0    23.62 0  
-1 0   R  
0  0   R  
21 13.84 RF  0   0    27.62 0  

*END*
