*PADS-LIBRARY-PCB-DECALS-V9*

LC-LFCSP-40_6X6X05P I 0     0     2 2 7 0 41 22 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-135.86 143.86 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-135.86 193.86 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 9.84 26 -1
-116.14 -144.68
-125.98 -144.68
CIRCLE 2 3.94 18 -1
-70.87 -89.57
-110.24 -89.57
OPEN   3 7.87 26 -1
-109.25 -126.97
-126.97 -126.97
-126.97 -109.25
OPEN   3 7.87 26 -1
109.25 -126.97
126.97 -126.97
126.97 -109.25
OPEN   3 7.87 26 -1
109.25 126.97
126.97 126.97
126.97 109.25
OPEN   3 7.87 26 -1
-109.25 126.97
-126.97 126.97
-126.97 109.25
CLOSED 5 3.94 18 -1
121.06 -121.06
121.06 121.06
-121.06 121.06
-121.06 -121.06
121.06 -121.06
T0     0     0     0     0
T-88.58 -118.11 -88.58 -118.11 1
T-68.9 -118.11 -68.9 -118.11 2
T-49.21 -118.11 -49.21 -118.11 3
T-29.53 -118.11 -29.53 -118.11 4
T-9.84 -118.11 -9.84 -118.11 5
T9.84  -118.11 9.84  -118.11 6
T29.53 -118.11 29.53 -118.11 7
T49.21 -118.11 49.21 -118.11 8
T68.9  -118.11 68.9  -118.11 9
T88.58 -118.11 88.58 -118.11 10
T118.11 -88.58 118.11 -88.58 11
T118.11 -68.9 118.11 -68.9 12
T118.11 -49.21 118.11 -49.21 13
T118.11 -29.53 118.11 -29.53 14
T118.11 -9.84 118.11 -9.84 15
T118.11 9.84  118.11 9.84  16
T118.11 29.53 118.11 29.53 17
T118.11 49.21 118.11 49.21 18
T118.11 68.9  118.11 68.9  19
T118.11 88.58 118.11 88.58 20
T88.58 118.11 88.58 118.11 21
T68.9  118.11 68.9  118.11 22
T49.21 118.11 49.21 118.11 23
T29.53 118.11 29.53 118.11 24
T9.84  118.11 9.84  118.11 25
T-9.84 118.11 -9.84 118.11 26
T-29.53 118.11 -29.53 118.11 27
T-49.21 118.11 -49.21 118.11 28
T-68.9 118.11 -68.9 118.11 29
T-88.58 118.11 -88.58 118.11 30
T-118.11 88.58 -118.11 88.58 31
T-118.11 68.9  -118.11 68.9  32
T-118.11 49.21 -118.11 49.21 33
T-118.11 29.53 -118.11 29.53 34
T-118.11 9.84  -118.11 9.84  35
T-118.11 -9.84 -118.11 -9.84 36
T-118.11 -29.53 -118.11 -29.53 37
T-118.11 -49.21 -118.11 -49.21 38
T-118.11 -68.9 -118.11 -68.9 39
T-118.11 -88.58 -118.11 -88.58 40
PAD 1 4 P 0    
-2 169.29 RF  0   90   169.29 0  
-1 0   R  
0  0   R  
21 173.29 RF  0   90   173.29 0  
PAD 0 4 P 0    
-2 11.02 OF  90   31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  90   35.5 0  
PAD 12 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 13 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 14 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 15 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 16 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 17 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 18 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 19 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 20 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 21 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 32 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 33 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 34 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 35 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 36 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 37 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 38 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 39 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 40 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  
PAD 41 4 P 0    
-2 11.02 OF  0    31.5 0  
-1 0   R  
0  0   R  
21 15.02 OF  0    35.5 0  

*END*
