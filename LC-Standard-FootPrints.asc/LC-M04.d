*PADS-LIBRARY-PCB-DECALS-V9*

LC-M04           I 0     0     2 2 5 0 4 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-55.28 70.17 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-55.28 120.17 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 10 26 -1
-40.28 52.17
-50.28 52.17
CIRCLE 2 3.94 18 -1
-5.91 26.57
-19.69 26.57
CLOSED 5 3.94 18 -1
-25.59 39.37
25.59 39.37
25.59 -39.37
-25.59 -39.37
-25.59 39.37
OPEN   2 7.87 26 -1
-27.56 -47.24
27.56 -47.24
OPEN   2 7.87 26 -1
-27.56 47.24
27.56 47.24
T-31.5 -25.59 -31.5 -25.59 1
T-31.5 23.62 -31.5 23.62 2
T31.5  25.59 31.5  25.59 3
T31.5  -25.59 31.5  -25.59 4
PAD 0 3 P 0    
-2 15.75 RF  0   0    39.37 0  
-1 0   R  
0  0   R  
PAD 2 3 P 0    
-2 23.62 RF  0   0    39.37 0  
-1 0   R  
0  0   R  

*END*
