*PADS-LIBRARY-PCB-DECALS-V9*

LC-SMBF_S1       I 0     0     2 2 7 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-145.73 95.68 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-145.73 145.68 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
-86.61 -74.8
86.61 -74.8
86.61 74.8 
-86.61 74.8 
-86.61 -74.8
OPEN   4 10 26 -1
-98.43 62.99
-98.43 82.68
98.43 82.68
98.43 62.99
OPEN   4 10 26 -1
-98.43 -62.99
-98.43 -82.68
98.43 -82.68
98.43 -62.99
OPEN   2 10 26 -1
-33.46 -82.68
-33.46 82.68
OPEN   2 8 19 -1
-141.73 -19.68
-141.73 19.68
OPEN   2 8 19 -1
161.42 -19.68
161.42 19.68
OPEN   2 8 19 -1
141.73 0    
181.1 0    
T86.61 0     86.61 0     1
T-86.61 0     -86.61 0     2
PAD 0 3 P 0    
-2 78.74 RF  0   90   98.43 0  
-1 0   R  
0  0   R  

*END*
