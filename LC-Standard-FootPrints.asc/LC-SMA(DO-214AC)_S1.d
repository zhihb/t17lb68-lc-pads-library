*PADS-LIBRARY-PCB-DECALS-V9*

LC-SMA(DO-214AC)_S1 I 0     0     2 2 7 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-141.8 72.06 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-141.8 122.06 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
-85.04 -51.18
-85.04 51.18
85.04 51.18
85.04 -51.18
-85.04 -51.18
OPEN   4 10 26 -1
-92.52 -51.18
-92.52 -59.06
92.52 -59.06
92.52 -51.18
OPEN   2 10 26 -1
-29.53 -59.06
-29.53 59.06
OPEN   4 10 26 -1
-92.52 51.18
-92.52 59.06
92.52 59.06
92.52 51.18
OPEN   2 8 19 -1
-137.8 -19.68
-137.8 19.68
OPEN   2 8 19 -1
153.54 -19.69
153.54 19.68
OPEN   2 8 19 -1
133.86 -0   
173.23 -0   
T82.68 0     82.68 0     1
T-82.68 0     -82.68 0     2
PAD 0 3 P 0    
-2 78.74 RF  0   90   78.74 0  
-1 0   R  
0  0   R  

*END*
