*PADS-LIBRARY-PCB-DECALS-V9*

LC-TO-220-5      I 0     0     2 2 3 0 5 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-213.66 142.92 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-213.66 192.92 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 15.75 26 -1
-177.17 0    
-192.91 0    
CLOSED 5 10 26 -1
-208.66 -62.99
-208.66 129.92
208.66 129.92
208.66 -62.99
-208.66 -62.99
OPEN   2 10 26 -1
-204.72 110.24
208.66 110.24
T-133.86 0     -133.86 0     1
T-66.93 0     -66.93 0     2
T0     0     0     0     3
T66.93 0     66.93 0     4
T133.86 0     133.86 0     5
PAD 1 5 P 47.24
-2 57.09 RF  0   90   86.61 0  
-1 57.09 RF  0   90   86.61 0  
0  57.09 RF  0   90   86.61 0  
21 61.09 RF  0   90   90.61 0  
28 61.09 RF  0   90   90.61 0  
PAD 0 5 P 47.24
-2 57.09 OF  90   86.61 0  
-1 57.09 OF  90   86.61 0  
0  57.09 OF  90   86.61 0  
21 61.09 OF  90   90.61 0  
28 61.09 OF  90   90.61 0  

*END*
