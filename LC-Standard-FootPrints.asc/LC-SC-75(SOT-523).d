*PADS-LIBRARY-PCB-DECALS-V9*

LC-SC-75(SOT-523) I 0     0     2 2 2 0 3 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-40.35 39.5  0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-40.35 89.5  0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   3 7.87 26 -1
-15.75 -19.68
-15.75 -27.56
3.94  -27.56
OPEN   3 7.87 26 -1
3.94  27.56
-15.75 27.56
-15.75 19.68
T26.57 -19.68 26.57 -19.68 1
T26.57 19.69 26.57 19.69 2
T-26.57 0     -26.57 0     3
PAD 0 3 P 0    
-2 15.75 RF  0   0    23.62 0  
-1 0   R  
0  0   R  
PAD 3 3 P 0    
-2 15.75 RF  0   0    27.56 0  
-1 0   R  
0  0   R  

*END*
