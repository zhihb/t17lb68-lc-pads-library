*PADS-LIBRARY-PCB-DECALS-V9*

LC-VDFPN-8(MLP-8) I 0     0     2 2 5 0 9 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-130.98 185.95 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-130.98 235.95 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 15.75 26 -1
-106.3 -196.85
-122.05 -196.85
CIRCLE 2 3.94 18 -1
-66.93 -122.05
-106.3 -122.05
CLOSED 5 3.94 18 -1
-118.11 -157.48
-118.11 157.48
118.11 157.48
118.11 -157.48
-118.11 -157.48
OPEN   4 10 26 -1
-102.36 165.35
-125.98 165.35
-125.98 -165.35
-102.36 -165.35
OPEN   4 10 26 -1
102.36 165.35
125.98 165.35
125.98 -165.35
102.36 -165.35
T0     0     0     0     0
T-75   -154.33 -75   -154.33 1
T-25   -154.33 -25   -154.33 2
T25    -154.33 25    -154.33 3
T75    -154.33 75    -154.33 4
T75    154.33 75    154.33 5
T25    154.33 25    154.33 6
T-25   154.33 -25   154.33 7
T-75   154.33 -75   154.33 8
PAD 1 3 P 0    
-2 188.98 RF  0   90   204.72 0  
-1 0   R  
0  0   R  
PAD 0 3 P 0    
-2 27.56 RF  0   90   47.24 0  
-1 0   R  
0  0   R  

*END*
