*PADS-LIBRARY-PCB-DECALS-V9*

LC-TO-220-7C     I 0     0     2 2 6 0 6 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-215.63 142.92 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-215.63 192.92 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 15.75 26 -1
-169.29 -127.51
-185.04 -127.51
OPEN   2 10 26 -1
150.39 -137.8
150.39 -55.12
OPEN   4 10 26 -1
-106.3 -55.12
-210.63 -55.12
-210.63 129.92
-210.63 110.24
OPEN   8 10 26 -1
210.63 110.24
210.63 129.92
210.63 110.24
-210.63 110.24
-210.63 129.92
210.63 129.92
210.63 -55.12
106.3 -55.12
OPEN   2 10 26 -1
-149.61 -137.8
-149.61 -55.12
OPEN   6 10 18 -1
106.3 -55.12
210.63 -55.12
210.63 129.92
-210.63 129.92
-210.63 -55.12
-106.3 -55.12
T-150  -200  -150  -200  1
T-100  50    -100  50    2
T-50   -50   -50   -50   3
T0     50    0     50    4
T50    -50   50    -50   5
T150   -200  150   -200  7
PAD 1 3 P 47.24
-2 78.74 S   0  
-1 78.74 S   0  
0  78.74 S   0  
PAD 0 3 P 47.24
-2 78.74 R  
-1 78.74 R  
0  78.74 R  

*END*
