*PADS-LIBRARY-PCB-DECALS-V9*

LC-SOD-323       I 0     0     2 2 6 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-76.77 35.56 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-76.77 85.56 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
33.46 -24.61
33.46 24.61
-33.46 24.61
-33.46 -24.61
33.46 -24.61
CLOSED 5 7.87 26 -1
-15.75 23.62
15.75 23.62
15.75 -23.62
-15.75 -23.62
-15.75 23.62
OPEN   2 7.87 26 -1
-5.12 -23.62
-5.12 23.62
OPEN   2 3.94 19 -1
-74.8 -11.81
-74.8 11.81
OPEN   2 3.94 19 -1
86.61 -11.81
86.61 11.81
OPEN   2 3.94 19 -1
74.8  0    
98.43 0    
T46.46 0     46.46 0     1
T-46.46 0     -46.46 0     2
PAD 0 3 P 0    
-2 32.68 RF  0   0    39.37 0  
-1 0   R  
0  0   R  

*END*
