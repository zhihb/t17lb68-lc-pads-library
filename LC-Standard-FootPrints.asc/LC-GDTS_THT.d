*PADS-LIBRARY-PCB-DECALS-V9*

LC-GDTS_THT      I 0     0     2 2 3 1 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-235.43 123   0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-235.43 173   0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 10 26 -1
-120  110  
120   110  
120   -110 
-120  -110 
-120  110  
OPEN   2 10 26 -1
-149.61 0    
-120  0    
OPEN   2 10 26 -1
120   0    
149.61 0    
-78.74 -23.62 0 26  50   10 0 0 0 0 "Regular <Romansim Stroke Font>"
GDTs
T-200  0     -200  0     1
T200   0     200   0     2
PAD 0 3 P 43.31
-2 70.87 R  
-1 70.87 R  
0  70.87 R  

*END*
