*PADS-LIBRARY-PCB-DECALS-V9*

LC-TO-225        I 0     0     2 2 2 0 3 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-158.54 83.87 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-158.54 133.87 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   2 10 26 -1
-153.54 55.12
153.54 55.12
CLOSED 5 10 26 -1
-153.54 -55.12
153.54 -55.12
153.54 70.87
-153.54 70.87
-153.54 -55.12
T-100  0     -100  0     1
T-0    0     -0    0     2
T100   0     100   0     3
PAD 1 3 P 51.18
-2 78.74 RF  0   0    78.74 0  
-1 78.74 RF  0   0    78.74 0  
0  78.74 RF  0   0    78.74 0  
PAD 0 3 P 51.18
-2 78.74 R  
-1 78.74 R  
0  78.74 R  

*END*
