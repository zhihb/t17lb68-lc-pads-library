*PADS-LIBRARY-PCB-DECALS-V9*

LC-SMAF_S3       I 0     0     2 2 6 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-137.86 65.09 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-137.86 115.09 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
74.8  -55.12
74.8  55.12
-74.8 55.12
-74.8 -55.12
74.8  -55.12
CLOSED 5 10 26 -1
23.62 -47.24
23.62 47.24
-23.62 47.24
-23.62 -47.24
23.62 -47.24
OPEN   2 10 26 -1
-9.84 -47.24
-9.84 47.24
OPEN   2 8 19 -1
129.92 0    
169.29 0    
OPEN   2 8 19 -1
149.61 -19.68
149.61 19.68
OPEN   2 8 19 -1
-133.86 -19.68
-133.86 19.68
T78.74 0     78.74 0     1
T-78.74 0     -78.74 0     2
PAD 0 3 P 0    
-2 70.87 RF  0   0    78.74 0  
-1 0   R  
0  0   R  

*END*
