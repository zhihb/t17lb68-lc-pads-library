*PADS-LIBRARY-PCB-DECALS-V9*

LC-P600          I 0     0     2 2 7 0 2 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-349.21 194.1 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-349.21 244.1 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   4 10 26 -1
-153.54 -181.1
-153.54 181.1
-145.67 181.1
-145.67 -181.1
OPEN   2 10 26 -1
181.1 0    
224.41 0    
OPEN   2 10 26 -1
-228.35 0    
-181.1 0    
CLOSED 5 10 26 -1
181.1 -181.1
181.1 181.1
-181.1 181.1
-181.1 -181.1
181.1 -181.1
OPEN   2 8 19 -1
-318.9 -66.93
-279.53 -66.93
OPEN   2 8 19 -1
283.46 -82.68
322.83 -82.68
OPEN   2 8 19 -1
303.15 -102.36
303.15 -62.99
T300   0     300   0     1
T-300  0     -300  0     2
PAD 1 3 P 59.06
-2 98.43 S   0  
-1 98.43 S   0  
0  98.43 S   0  
PAD 2 3 P 59.06
-2 98.43 R  
-1 98.43 R  
0  98.43 R  

*END*
