*PADS-LIBRARY-PCB-DECALS-V9*

LC-SSOP-20_150MIL I 0     0     2 2 3 0 20 11 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-175.2 145.83 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-175.2 195.83 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 7 10 26 -1
-167.91 -19.68 2700 1800 -187.6 -19.68 -148.23 19.68
-167.91 19.68
-167.91 59.06
169.29 59.06
169.29 -59.06
-167.91 -59.06
-167.91 -19.68
CIRCLE 2 11.81 26 -1
-134.84 -115.16
-146.65 -115.16
CLOSED 5 3.94 18 -1
-173.23 -78.74
173.23 -78.74
173.23 78.74
-173.23 78.74
-173.23 -78.74
T-112.5 -103.74 -112.5 -103.74 1
T-87.5 -103.74 -87.5 -103.74 2
T-62.5 -103.74 -62.5 -103.74 3
T-37.5 -103.74 -37.5 -103.74 4
T-12.5 -103.74 -12.5 -103.74 5
T12.5  -103.74 12.5  -103.74 6
T37.5  -103.74 37.5  -103.74 7
T62.5  -103.74 62.5  -103.74 8
T87.5  -103.74 87.5  -103.74 9
T112.5 -103.74 112.5 -103.74 10
T112.5 103.74 112.5 103.74 11
T87.5  103.74 87.5  103.74 12
T62.5  103.74 62.5  103.74 13
T37.5  103.74 37.5  103.74 14
T12.5  103.74 12.5  103.74 15
T-12.5 103.74 -12.5 103.74 16
T-37.5 103.74 -37.5 103.74 17
T-62.5 103.74 -62.5 103.74 18
T-87.5 103.74 -87.5 103.74 19
T-112.5 103.74 -112.5 103.74 20
PAD 0 4 P 0    
-2 15.75 OF  90   64.17 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   68.17 0  
PAD 11 4 P 0    
-2 15.75 OF  90   64.17 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   68.17 0  
PAD 12 4 P 0    
-2 15.75 OF  90   64.17 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   68.17 0  
PAD 13 4 P 0    
-2 15.75 OF  90   64.17 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   68.17 0  
PAD 14 4 P 0    
-2 15.75 OF  90   64.17 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   68.17 0  
PAD 15 4 P 0    
-2 15.75 OF  90   64.17 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   68.17 0  
PAD 16 4 P 0    
-2 15.75 OF  90   64.17 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   68.17 0  
PAD 17 4 P 0    
-2 15.75 OF  90   64.17 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   68.17 0  
PAD 18 4 P 0    
-2 15.75 OF  90   64.17 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   68.17 0  
PAD 19 4 P 0    
-2 15.75 OF  90   64.17 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   68.17 0  
PAD 20 4 P 0    
-2 15.75 OF  90   64.17 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   68.17 0  

*END*
