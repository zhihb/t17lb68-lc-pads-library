*PADS-LIBRARY-PCB-DECALS-V9*

LC-SOT-223-6     I 0     0     2 2 2 0 6 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-165.35 141.86 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-165.35 191.86 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
-72.83 131.89
72.83 131.89
72.83 -131.89
-72.83 -131.89
-72.83 131.89
CLOSED 5 10 26 -1
-55.12 118.11
55.12 118.11
55.12 -118.11
-55.12 -118.11
-55.12 118.11
T122.05 -100  122.05 -100  1
T122.05 -50   122.05 -50   2
T122.05 0     122.05 0     3
T122.05 50    122.05 50    4
T122.05 100   122.05 100   5
T-122.05 0     -122.05 0     6
PAD 0 3 P 0    
-2 27.56 RF  0   0    86.61 0  
-1 0   R  
0  0   R  
PAD 6 3 P 0    
-2 86.61 RF  0   90   137.8 0  
-1 0   R  
0  0   R  

*END*
