*PADS-LIBRARY-PCB-DECALS-V9*

LC-QFN-6_3X3X095P I 0     0     2 2 4 0 7 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-71.85 80.83 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-71.85 130.83 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 11.81 26 -1
-56.1 -78.74
-65.94 -78.74
CLOSED 5 3.94 18 -1
-59.05 -59.06
-59.05 59.06
59.06 59.06
59.06 -59.06
-59.05 -59.06
OPEN   2 7.87 26 -1
-61.02 -58.07
-61.02 60.04
OPEN   2 7.87 26 -1
61.02 -59.06
61.02 59.06
T0     0     0     0     0
T-37.4 -57.09 -37.4 -57.09 1
T0     -57.09 0     -57.09 2
T37.4  -57.09 37.4  -57.09 3
T37.4  57.09 37.4  57.09 4
T0     57.09 0     57.09 5
T-37.4 57.09 -37.4 57.09 6
PAD 1 3 P 0    
-2 57.09 RF  0   0    90.55 0  
-1 0   R  
0  0   R  
PAD 0 3 P 0    
-2 17.72 OF  90   31.5 0  
-1 0   R  
0  0   R  

*END*
