*PADS-LIBRARY-PCB-DECALS-V9*

LC-SOT-223-5     I 0     0     2 2 2 0 5 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-163.39 141.86 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-163.39 191.86 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CLOSED 5 3.94 18 -1
-70.87 131.89
74.8  131.89
74.8  -131.89
-70.87 -131.89
-70.87 131.89
CLOSED 5 10 26 -1
-53.15 118.11
57.09 118.11
57.09 -118.11
-53.15 -118.11
-53.15 118.11
T120.08 -88.58 120.08 -88.58 1
T120.08 -29.53 120.08 -29.53 2
T120.08 29.53 120.08 29.53 3
T120.08 88.58 120.08 88.58 4
T-120.08 0     -120.08 0     5
PAD 0 3 P 0    
-2 35.43 RF  0   0    86.61 0  
-1 0   R  
0  0   R  
PAD 5 3 P 0    
-2 86.61 RF  0   90   137.8 0  
-1 0   R  
0  0   R  

*END*
