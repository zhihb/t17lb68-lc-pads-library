*PADS-LIBRARY-PCB-DECALS-V9*

LC-SMBF_S4       I 0     0     2 2 6 0 2 1 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-185.1 145.68 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
-185.1 95.68 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
CLOSED 5 3.94 18 -1
86.61 -74.8
86.61 74.8 
-86.61 74.8 
-86.61 -74.8
86.61 -74.8
CLOSED 5 10 26 -1
139.76 -82.68
139.76 82.68
-139.76 82.68
-139.76 -82.68
139.76 -82.68
OPEN   2 8 19 -1
-181.1 -19.68
-181.1 19.68
OPEN   2 8 19 -1
173.23 -19.68
173.23 19.68
OPEN   2 8 19 -1
153.54 0    
192.91 0    
COPCLS 5 10 26 -1
-169.29 -62.99
-153.54 -62.99
-153.54 62.99
-169.29 62.99
-169.29 -62.99
T86.61 0     86.61 0     1
T-86.61 0     -86.61 0     2
PAD 0 3 P 0    
-2 78.74 RF  0   90   98.43 0  
-1 0   R  
0  0   R  

*END*
