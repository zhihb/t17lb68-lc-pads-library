*PADS-LIBRARY-PCB-DECALS-V9*

LC-TO-92-3L      I 0     0     2 2 2 0 3 2 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-102.8 110.8 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-102.8 160.8 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
OPEN   3 10 26 -1
-77.95 -59.06 2171 -2542 -97.8 -97.8 97.8  97.8 
77.95 -59.06
-77.95 -59.05
OPEN   3 3.94 18 -1
-77.95 -59.06 2171 -2542 -97.8 -97.8 97.8  97.8 
77.95 -59.06
-77.95 -59.05
T-50   0     -50   0     1
T0     0     0     0     2
T50    0     50    0     3
PAD 0 5 P 27.56
-2 39.37 OF  90   78.74 0  
-1 39.37 OF  90   78.74 0  
0  39.37 OF  90   78.74 0  
21 43.37 OF  90   82.74 0  
28 43.37 OF  90   82.74 0  
PAD 2 5 P 27.56
-2 39.37 OF  90   78.74 0  
-1 39.37 OF  90   78.74 0  
0  39.37 OF  90   78.74 0  
21 43.37 OF  90   82.74 0  
28 43.37 OF  90   82.74 0  

*END*
