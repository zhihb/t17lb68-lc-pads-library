*PADS-LIBRARY-PCB-DECALS-V9*

LC-POWERSO-36    I 0     0     2 2 8 0 38 21 0
TIMESTAMP 2017.04.26.03.27.48
"WEBSITE" https://git.oschina.net/fanniu/t17lb68-lc-pads-library
"PACKAGE_VER" V1
-327.83 315.12 0     0 50    5     26 0 33 "Regular <Romansim Stroke Font>"
Comment
-327.83 365.12 0     0 50    5     26 0 34 "Regular <Romansim Stroke Font>"
REF-DES
CIRCLE 2 11.81 26 -1
-239.17 -282.48
-250.98 -282.48
CIRCLE 2 23.62 26 -1
-275.59 -192.91
-299.21 -192.91
CIRCLE 2 3.94 18 -1
-251.97 -181.1
-299.21 -181.1
CLOSED 5 3.94 18 -1
-314.96 -220.47
-314.96 220.47
314.96 220.47
314.96 -220.47
-314.96 -220.47
OPEN   3 10 26 -1
240.16 228.35
322.83 228.35
322.83 90.55
OPEN   3 10 26 -1
240.16 -228.35
322.83 -228.35
322.83 -90.55
OPEN   3 10 26 -1
-322.83 90.55
-322.83 228.35
-240.16 228.35
OPEN   3 10 26 -1
-322.83 -90.55
-322.83 -228.35
-240.16 -228.35
T0     0     0     0     0
T0     0     0     0     0/2
T-217.52 -265.75 -217.52 -265.75 1
T-191.93 -265.75 -191.93 -265.75 2
T-166.34 -265.75 -166.34 -265.75 3
T-140.75 -265.75 -140.75 -265.75 4
T-115.16 -265.75 -115.16 -265.75 5
T-89.57 -265.75 -89.57 -265.75 6
T-63.98 -265.75 -63.98 -265.75 7
T-38.39 -265.75 -38.39 -265.75 8
T-12.8 -265.75 -12.8 -265.75 9
T12.8  -265.75 12.8  -265.75 10
T38.39 -265.75 38.39 -265.75 11
T63.98 -265.75 63.98 -265.75 12
T89.57 -265.75 89.57 -265.75 13
T115.16 -265.75 115.16 -265.75 14
T140.75 -265.75 140.75 -265.75 15
T166.34 -265.75 166.34 -265.75 16
T191.93 -265.75 191.93 -265.75 17
T217.52 -265.75 217.52 -265.75 18
T217.52 265.75 217.52 265.75 19
T191.93 265.75 191.93 265.75 20
T166.34 265.75 166.34 265.75 21
T140.75 265.75 140.75 265.75 22
T115.16 265.75 115.16 265.75 23
T89.57 265.75 89.57 265.75 24
T63.98 265.75 63.98 265.75 25
T38.39 265.75 38.39 265.75 26
T12.8  265.75 12.8  265.75 27
T-12.8 265.75 -12.8 265.75 28
T-38.39 265.75 -38.39 265.75 29
T-63.98 265.75 -63.98 265.75 30
T-89.57 265.75 -89.57 265.75 31
T-115.16 265.75 -115.16 265.75 32
T-140.75 265.75 -140.75 265.75 33
T-166.34 265.75 -166.34 265.75 34
T-191.93 265.75 -191.93 265.75 35
T-217.52 265.75 -217.52 265.75 36
PAD 1 3 P 0    
-2 157.48 RF  0   0    629.92 0  
-1 0   R  
0  0   R  
PAD 2 3 P 0    
-2 275.59 RF  0   0    393.7 0  
-1 0   R  
0  0   R  
PAD 0 4 P 0    
-2 15.75 OF  90   78.74 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   82.74 0  
PAD 21 4 P 0    
-2 15.75 OF  90   78.74 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   82.74 0  
PAD 22 4 P 0    
-2 15.75 OF  90   78.74 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   82.74 0  
PAD 23 4 P 0    
-2 15.75 OF  90   78.74 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   82.74 0  
PAD 24 4 P 0    
-2 15.75 OF  90   78.74 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   82.74 0  
PAD 25 4 P 0    
-2 15.75 OF  90   78.74 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   82.74 0  
PAD 26 4 P 0    
-2 15.75 OF  90   78.74 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   82.74 0  
PAD 27 4 P 0    
-2 15.75 OF  90   78.74 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   82.74 0  
PAD 28 4 P 0    
-2 15.75 OF  90   78.74 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   82.74 0  
PAD 29 4 P 0    
-2 15.75 OF  90   78.74 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   82.74 0  
PAD 30 4 P 0    
-2 15.75 OF  90   78.74 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   82.74 0  
PAD 31 4 P 0    
-2 15.75 OF  90   78.74 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   82.74 0  
PAD 32 4 P 0    
-2 15.75 OF  90   78.74 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   82.74 0  
PAD 33 4 P 0    
-2 15.75 OF  90   78.74 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   82.74 0  
PAD 34 4 P 0    
-2 15.75 OF  90   78.74 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   82.74 0  
PAD 35 4 P 0    
-2 15.75 OF  90   78.74 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   82.74 0  
PAD 36 4 P 0    
-2 15.75 OF  90   78.74 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   82.74 0  
PAD 37 4 P 0    
-2 15.75 OF  90   78.74 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   82.74 0  
PAD 38 4 P 0    
-2 15.75 OF  90   78.74 0  
-1 0   R  
0  0   R  
21 19.75 OF  90   82.74 0  

*END*
